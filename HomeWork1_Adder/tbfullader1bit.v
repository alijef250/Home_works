module tb1bitFullAdder;

    reg i1;
    reg i2;
    reg ic;

    wire os;
    wire oc;

    tb1bitFullAdder uut (.i1(i1), .i2(i2), .ic(ic), .os(os), .oc(oc));

        initial begin
            
            $dumpfile("test.vcd");
            $dumpvars(0, tb1bitFullAdder);

            i1 = 1'b0;
            i2 = 1'b0;
            ic = 1'b0;

            #10
            i1 = 1'b1;

            #10
            ic = 1'b1;

     
            $finish;

        end

endmodule
