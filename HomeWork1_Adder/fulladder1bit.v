module 1bitFullAdder(i1, i2, i3, os, oc);

    input i1, i2, i3;
    output os, oc;
    wire w1, w2, w3;

    assign w1 = i1 ^ i2;
    assign os = w1 ^ ic;
    assign w2 = w1 & ic;
    assign w3 = i1 & i2;
    assign oc = w2 | w3;

endmodule
